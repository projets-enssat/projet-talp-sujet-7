from src.prepare_data import prepare_data
from src.parser import parse_args
from src.model import Model
from src.models import Model1


def train_model(ModelClass):
    X_train, X_test, y_train, y_test, phonem_columns = Model.helper.get_splitted_data()

    model = ModelClass(len(phonem_columns))

    history = model.learn(
        X_train, y_train,
        batch_size=80,
        epochs=800
    )


def test_model(ModelClass):
    X_train, X_test, y_train, y_test, phonem_columns = Model.helper.get_splitted_data()

    model = ModelClass(len(phonem_columns))
    model.load_weights()

    model.test(X_test, y_test)


models = {
    'model1': Model1
}


def main():
    args = parse_args()
    if args.command == "generate-dataset":
        prepare_data(max_file=args.maxi)
    elif args.command == "train":
        train_model(models[args.model])
    elif args.command == "test":
        test_model(models[args.model])


if __name__ == "__main__":
    main()
