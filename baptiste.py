import os, glob
import numpy as np
import pandas as pd

import tensorflow as tf
from sklearn.model_selection import train_test_split #pip install sklearn
from sklearn.preprocessing import StandardScaler

from baptiste.logger import logger
from baptiste.models import get_compiled_model
from baptiste.analyse import apply_analyse_avg
from baptiste.settings import DATASETS_FOLDER

def denormalize(nf0, mini, maxi): return nf0 * (maxi - mini) + mini

if __name__ == "__main__":
    
    # load the dataset
    logger.info("reading datafiles...")
    files = glob.glob(os.path.join(DATASETS_FOLDER, "*.csv"))
    dataset = pd.concat(
        (pd.read_csv(f, sep=";", header=0) for f in files[:1000]),
        ignore_index=True
    )

    
    # applying modifications with f0 averaged
    logger.info("cleaning dataset...")
    dataset, f_min, f_max = apply_analyse_avg(dataset)
    len_dataset = len(dataset)

    targets = dataset.pop('avg_f0')
    dataset_train, dataset_test, target_train, target_test = train_test_split(
        dataset, targets, train_size=0.2, random_state=0
    )

    # using tensorflow dataset
    dataset_train = tf.data.Dataset.from_tensor_slices((dataset_train.values, target_train.values))
    dataset_test = tf.data.Dataset.from_tensor_slices((dataset_test.values, target_test.values))

    dataset_train = dataset_train.shuffle(len_dataset).batch(100)
    dataset_test = dataset_test.shuffle(len_dataset).batch(1)

    def get_compiled_model()->tf.keras.Sequential:
        model = tf.keras.Sequential([
            tf.keras.layers.Dense(32, activation='relu'),
            tf.keras.layers.Dense(14, activation='elu'),
            tf.keras.layers.Dense(10, activation='relu'),
            tf.keras.layers.Dense(1, activation='tanh'),
        ])

        model.compile(optimizer='sgd',
                        loss='mean_squared_error')
        return model
    
    model = get_compiled_model()
    model.save("data/models/baptiste.h5")
    model.fit(dataset_train, epochs=100)


    ######### 
    res = model.predict(dataset_test)
    ecarts = []
    for k, out in enumerate(dataset_test.as_numpy_iterator()):
        pf = denormalize(out[1][0], f_min, f_max)
        ef = denormalize(res[k][0], f_min, f_max)
        ecarts.append(
            abs(pf-ef)
        )
    print(np.array(ecarts).mean())