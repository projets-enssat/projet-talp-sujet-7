import os
import re
import io
import json
import pandas

import numpy as np
from tqdm import trange

from src.headers import LABEL_PHONE_HEADERS
from src.logger import logger
from sklearn import preprocessing


LF0_TXT_FOLDER = 'data/lf0_txt'
DATASET_FOLDER = 'data/datasets'

LABEL_PHONE_ALIGN_FOLDER = "data/label_phone_align"
LABEL_PHONE_ALIGN_CSV_FOLDER = "data/label_phone_align_csv"
LAB_PATTERN = re.compile(
    "(\d+) (\d+) ([^^]+)\^([^-]+)-([^+]+)\+([^=]+)=([^@]+|@)@([^_]+)_([^\/]+)\/A:([^_]+)_([^_]+)_([^\/]+)\/B:([^-]+)-([^-]+)-([^@]+|@)@([^-]+)-([^&]+)&([^-]+)-([^#]+)#([^-]+)-([^$]+)\$([^-]+)-([^!]+)!([^-]+)-([^;]+);([^-]+)-([^|]+)\|([^\/]*\/?)\/C:([^+]+)\+([^+]+)\+([^\/]+)\/D:([^_]+)_([^\/]+)\/E:([^+]+)\+([^@]+|@)@([^+]+)\+([^&]+)\&([^+]+)\+([^#]+)#([^+]+)\+([^\/]+)\/F:([^_]+)_([^\/]+)\/G:([^_]+)_([^\/]+)\/H:([^=]+)=([^^]+)\^([^=]+)=([^|]+)\|([^\/]+)\/I:([^_]+)_([^\/]+)\/J:([^+]+)\+([^-]+)-([^\/\n]+)"
)

MAX_LABEL_FILE = 7987


def load_label_phone(sample_id):
    filepath = os.path.join(LABEL_PHONE_ALIGN_FOLDER,
                            f'siwiss_{sample_id:04}.lab')
    rows = open(filepath).read().strip().split("\n")

    # trick to let pandas guess type of column data
    # generate in-memory csv
    csv_data = io.StringIO('\n'.join([
        # convert string phonem data in a list
        ';'.join(
            LAB_PATTERN.match(row).groups()
        ) for row in rows
    ]))
    # reset pointer at the beginning to read the entire file
    csv_data.seek(0)

    df = pandas.read_csv(csv_data, delimiter=';', header=None)
    df.columns = LABEL_PHONE_HEADERS

    return df


def load_frequencies(sample_id):
    return np.loadtxt(
        os.path.join(
            LF0_TXT_FOLDER,
            f'siwiss_{sample_id:04}.lf0'
        ), dtype=float
    )


def dataset_from_sample(sample_id):
    def workout_avg_f0(row):
        i0 = row['t0']//50000
        i1 = row['t1']//50000
        n = i1 - i0
        return np.average(f0[i0+n//4: i1-n//4])

    # load time phonems data
    df = load_label_phone(sample_id)

    # load fundamentals frequencies samples
    lf0 = load_frequencies(sample_id)
    f0 = np.array(list(map(lambda lf: pow(10, lf)//1000, lf0)))

    # add mean frequency value for each phonem
    df.insert(loc=0, column='avg_f0', value=pandas.Series(
        df.apply(workout_avg_f0, axis=1), dtype=int
    ))

    # save datasets in data/datasets folder
    return df


def normalize_data(df):
    # split categegorical features in binary features
    df = pandas.get_dummies(df)

    # remove f0 from normalization
    """
    columns = list(df.columns)
    columns.remove('avg_f0')

    avg_f0 = df.pop('avg_f0')
    print(avg_f0)
    """

    # prepare min max scaler for normalization
    scaler = preprocessing.MinMaxScaler()
    scaler.fit(df)

    # save f0 bounds in a separate file
    if0 = df.columns.get_loc('avg_f0')

    # save bounds in a separate file
    with open('data/datasets/f0_bounds.json', 'w') as bounds_file:
        json.dump([
            scaler.data_min_[if0],  # f0 min
            scaler.data_max_[if0]  # f0 max
        ], bounds_file)

    # normalize features values
    df = pandas.DataFrame(
        scaler.transform(df),
        columns=df.columns
    )

    #df['avg_f0'] = avg_f0.reset_index(drop=True)

    return df


def save_dataset(df, sample_id=None):
    df.to_csv(
        os.path.join(
            DATASET_FOLDER,
            'dataset.csv' if sample_id is None else f'dataset_{sample_id:04}.csv'
        ),
        sep=';',
        index=False
    )


def prepare_data(max_file=None):
    logger.info('Loading phonem data...')
    files = os.listdir(LABEL_PHONE_ALIGN_FOLDER)
    n = min(len(files), max_file) if max_file is not None else len(files)

    ldata = []
    for sample_id in trange(1, n+1):
        try:
            ldata.append(dataset_from_sample(sample_id))
        except FileNotFoundError:
            # certains index sont manquants -> flemme de chercher
            pass

    logger.info('Save concatenated data...')
    data = pandas.concat(ldata)
    # data = data[data.avg_f0 >= 0]

    #data = normalize_data(data)
    logger.info('Save dataset in data/datasets/dataset.csv')
    save_dataset(data)


def prepare_undummy_data(max_file=None):
    logger.info('Loading phonem data...')
    n = min(MAX_LABEL_FILE, max_file) if max_file is not None else MAX_LABEL_FILE

    ldata = []
    for sample_id in trange(1, n+1):
        try:
            ldata.append(dataset_from_sample(sample_id))
        except FileNotFoundError:
            # certains index sont manquants -> flemme de chercher
            pass

    logger.info('Concataining data...')
    data = pandas.concat(ldata, sort=False, ignore_index=True)

    logger.info('Saving...')
    save_dataset(data)


if __name__ == "__main__":
    prepare_undummy_data()
