import os
import wave
import math
import struct
import argparse

audio = []
sample_rate = 44100.0
parser = argparse.ArgumentParser()

parser.add_argument("filename", help="The name of the file.", type=str)
parser.add_argument("-p", "--path", help="The directory of the file.", type=str)


def append_silence(duration_milliseconds=500):
    """
    Adding silence is easy - we add zeros to the end of our array
    """
    num_samples = duration_milliseconds * (sample_rate / 1000.0)

    for _ in range(int(num_samples)):
        audio.append(0.0)
    pass


def append_sinewave(freq=440.0, duration_milliseconds=500, volume=1.0):
    """
    The sine wave generated here is the standard beep.  If you want something
    more aggresive you could try a square or saw tooth waveform.   Though there
    are some rather complicated issues with making high quality square and
    sawtooth waves... which we won't address here :) 
    """

    global audio  # using global variables isn't cool.

    num_samples = duration_milliseconds * (sample_rate / 1000.0)

    for x in range(int(num_samples)):
        audio.append(volume * math.sin(2 * math.pi * freq * (x / sample_rate)))


def save_wav(file_name):
    # Open up a wav file
    wav_file = wave.open(file_name, "w")

    # wav params
    nchannels = 1

    sampwidth = 2

    # 44100 is the industry standard sample rate - CD quality.  If you need to
    # save on file size you can adjust it downwards. The stanard for low quality
    # is 8000 or 8kHz.
    nframes = len(audio)
    comptype = "NONE"
    compname = "not compressed"
    wav_file.setparams((nchannels, sampwidth, sample_rate, nframes, comptype, compname))

    # WAV files here are using short, 16 bit, signed integers for the
    # sample size.  So we multiply the floating point data we have by 32767, the
    # maximum value for a short integer.  NOTE: It is theortically possible to
    # use the floating point -1.0 to 1.0 data directly in a WAV file but not
    # obvious how to do that using the wave module in python.
    for sample in audio:
        wav_file.writeframes(struct.pack("h", int(sample * 32767.0)))

    wav_file.close()


if __name__ == "__main__":

    args = parser.parse_args()
    filename = args.filename
    filepath = os.path.join(args.path, filename)

    with open(filepath, "r") as f:
        for f0 in f.readlines():
            if float(f0) == float("-1e+10"):
                append_silence(5)
            else:
                append_sinewave(float(f0) * 1000, 5)

        save_wav(filename + ".wav")
