import glob, os
import numpy as np
import pandas as pd
import plotly as plt

from sklearn.preprocessing import OneHotEncoder, QuantileTransformer

from src.logger import logger


DATASETS_FOLDER = 'data/datasets'


def drop_and_classify(df: pd.DataFrame):
    categorical_features = []
    continous_features = []

    for feature in df.columns.values:
        unique_values = df[feature].unique()

        if len(pd.value_counts(unique_values, dropna=True)) < 2 or feature in ['t0', 't1']:
            logger.debug(f'feature {feature} cannot be used')
            # df.drop(columns=[feature], inplace=True)
            continous_features.append(feature)
        else:
            try: 
                unique_values.astype(np.float)
                continous_features.append(feature)
            except ValueError:
                categorical_features.append(feature)
    
    return df, continous_features, categorical_features


CONTINUOUS_HEADERS = ['Count', 'Miss', 'Card', 'Min.', 'Qrt. 1', 'Mean', 'Median', 'Qrt. 3', 'Max.', 'Std. dev.']
CATEGORICAL_HEADERS = ['Count', 'Miss', 'Card', 'Mode 1', 'Mode 1 Freq.', 'Mode 1 %', 'Mode 2', 'Mode 2 Freq.', 'Mode 2 %']


def analyse_continous_feature(dataset: pd.DataFrame, features=[], save=False):
    df = pd.DataFrame(index=features, columns=CONTINUOUS_HEADERS)

    for feature in features:
        ds = dataset[feature] # pandas Serie

        df['Count'][feature] = ds.count()
        df['Miss'][feature] = ds.isna().sum()
        df['Card'][feature] = ds.nunique()

        df['Min.'][feature] = ds.min()
        df['Qrt. 1'][feature] = ds.quantile(0.25)
        df['Mean'][feature] = ds.mean()
        df['Median'][feature] = ds.median()
        df['Qrt. 3'][feature] = ds.quantile(0.75)
        df['Max.'][feature] = ds.max()
        df['Std. dev.'][feature] = ds.std()

    if save:
        df.to_csv("data/analyses/continous_analyse.csv", sep=";")

    return df


def analyse_categorical_feature(dataset: pd.DataFrame, features=[], save=False):
    df = pd.DataFrame(index=features, columns=CATEGORICAL_HEADERS)

    for feature in features:
        ds = dataset[feature] # pandas Serie

        df['Count'][feature] = ds.count()
        df['Miss'][feature] = ds.isna().sum()
        df['Card'][feature] = ds.nunique()

        vc = ds.value_counts()
        vcn = ds.value_counts(normalize=True)
        vkeys = vc.keys()
        #
        df['Mode 1'][feature] = vkeys[0]
        df['Mode 1 Freq.'][feature] = vc[0]
        df['Mode 1 %'][feature] = vcn[0]
        #
        try:
            df['Mode 2'][feature] = vkeys[1]
            df['Mode 2 Freq.'][feature] = vc[1]
            df['Mode 2 %'][feature] = vcn[1]
        except IndexError as e:
            df['Mode 2'][feature] = np.nan
            df['Mode 2 Freq.'][feature] = np.nan
            df['Mode 2 %'][feature] = np.nan

    if save:
        df.to_csv("data/analyses/categorical_analyse.csv", sep=";")

    return df


def make_charts(dataset: pd.DataFrame, analyse: pd.DataFrame, chart_name, features=[], bar_only=False):
    charts = plt.subplots.make_subplots(rows=28, cols=2, subplot_titles=features)
    for k, feature in enumerate(features):
        vc = dataset[feature].value_counts()

        if analyse['Card'][feature] < 10 and not bar_only:
            charts.append_trace(
                plt.graph_objs.Bar(x=vc.keys(), y=vc.values),
                row=1+k//2, col=k%2+1
            )
        else:
            charts.append_trace(
                plt.graph_objs.Histogram(x=vc.keys(), y=vc.values),
                row=1+k//2, col=k%2+1
            )
    
    charts['layout'].update(title=f'{chart_name} features', height=8000)
    plt.offline.plot(charts, filename=f'data/analyses/{chart_name}_features.html')


###########

def normalize_dataset(df: pd.DataFrame, continous=[], categorical=[]):

    continous_opt = QuantileTransformer()
    #continous.remove('avg_f0')
    df[continous] = continous_opt.fit_transform(df[continous])

    return df


def apply_analyse_avg(dataset: pd.DataFrame):
    """
    Applying modifications after analysing the dataset.
    - drop feature with one value inside it.
    """
    dataset, continous, categorical = drop_and_classify(dataset)
    dataset = dataset.dropna() # drop NaN values
    
    # transform string to numerical values
    #for cat in categorical:
    #    dataset[cat] = pd.Categorical(dataset[cat])
    #    dataset[cat] = dataset[cat].cat.codes
    
    dataset = normalize_dataset(dataset, continous=continous, categorical=categorical)

    dataset = pd.get_dummies(dataset, columns=categorical)
    return dataset


###########
if __name__ == "__main__":

    # TODO: move to args
    max_files = 7000

    # locate files
    # files = glob.glob(os.path.join(DATASETS_FOLDER, "dataset_*.csv"))
    
    logger.info("Reading dataset files from the dataset folder...")

    dataset = pd.read_csv("data/datasets/dataset.csv", sep=";", header=0, na_values='x')
    logger.info("The dataset is loadded!")

    dataset, continous, categorical = drop_and_classify(dataset)

    # analyses its
    continous_analyse = analyse_continous_feature(dataset, features=continous, save=True) # shortcut dataset.describe(include=[np.number])
    categorical_analyse = analyse_categorical_feature(dataset, features=categorical, save=True) # shortcut dataset.describe(iclude='O')

    # builds charts
    make_charts(dataset, continous_analyse, 'Continous', features=continous)
    make_charts(dataset, categorical_analyse, 'Categorical', features=categorical, bar_only=True)
