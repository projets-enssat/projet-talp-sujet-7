from src.prepare_data import normalize_data
from sklearn.model_selection import train_test_split
import json
import pandas
import numpy


class Model:
    name: str
    model = None

    class helper:
        @staticmethod
        def get_splitted_data(seed=0):
            # load dataset
            data = pandas.read_csv('data/datasets/dataset.csv', delimiter=';')
            data = normalize_data(data)
            # list binary features corresponding to phonems data
            phonem_columns = [col for col in data.columns]
            return (
                *train_test_split(data[phonem_columns],
                                  data['avg_f0'], test_size=0.33),
                phonem_columns
            )

    def __init__(self, size):
        self.model = self.create(size)

    @property
    def create(self):
        """
        Method to implement which define layers of the model
        """
        raise AttributeError("model has not been set")

    def save(self):
        self.model.save('data/models/%s.h5' % self.name)

    def load_weights(self):
        self.model.load_weights('data/models/%s.h5' % self.name)

    def learn(self, *args, save=True, **kwargs):
        history = self.model.fit(*args, **kwargs)

        if save:
            self.save()

    def test(self, X_test, y_test):
        mini, maxi = json.load(open('data/datasets/f0_bounds.json'))
        def denormalize(nf0): return nf0 * (maxi - mini) + mini

        normalized_predictions = self.model.predict(X_test)[:, 0]
        predicted_f0 = numpy.apply_along_axis(
            denormalize, 0, normalized_predictions)
        expected_f0 = numpy.apply_along_axis(denormalize, 0, y_test.values)

        ecarts = abs(predicted_f0 - expected_f0)

        print(json.dumps({
            "ecart moyen": ecarts.mean(),
            "deviation standard": ecarts.std(),
            "variance": ecarts.var()
        }, indent=4))
