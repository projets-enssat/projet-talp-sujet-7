import argparse
import sys


def parse_args():
    parser = argparse.ArgumentParser()
    subparsers = parser.add_subparsers()

    # clean parser
    prepare_parser = subparsers.add_parser(
        'generate-dataset',
        help='parse phonem and frequency datas, and concatenate in a single bundle CSV'
    )
    prepare_parser.set_defaults(command='generate-dataset')
    prepare_parser.add_argument(
        '--maxi', type=int, default=None, help='number of files to prepare')

    # learn parser
    learn_parser = subparsers.add_parser(
        'train',
        help='train a model based on phonem dataset'
    )
    learn_parser.set_defaults(command='train')
    learn_parser.add_argument('model')

    #
    learn_parser = subparsers.add_parser(
        'test',
        help='test a trained model on dataset'
    )
    learn_parser.set_defaults(command='test')
    learn_parser.add_argument('model')

    args = parser.parse_args()
    if getattr(args, 'command', None) is None:
        parser.print_help()
        sys.exit(2)
    return args
