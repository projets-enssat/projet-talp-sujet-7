from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense, Activation
from ..model import Model


class Model1(Model):
    name = "model1"

    def create(self, size):
        model = Sequential([
            Dense(32, input_shape=(size,), activation='relu'),
            Dense(14, activation='elu'),
            Dense(10, activation='relu'),
            Dense(1, activation='tanh'),
        ])

        model.compile(
            optimizer='sgd',
            loss='mean_squared_error'
        )

        return model
